# D I S T R O

This folder holds all the details for sharing your zine with others (for example, at dat://dat-zine-library.hashbase.io).  These details are kept mostly in your info.txt file.

# Editing your info.txt file.

The easiest thing to do is to just go into the existing one, click the edit pencil, and rewrite all the values while keeping the structure intact.  You want it to look like this:

```
title: your zine name
----
author: who you are
  (or alternatively)
authors: who you ares.
----
a thing: more about that thing.

```

You'll want to start with the title, and you'll want to make each section separated by `----`.  Other than that have at it!

````
# Your Cover Image

This can be any image(jpg, gif, png) that you've called `cover`.  It is used to display your zine on zine libraries.  I recommend it being no bigger than 500px high, cos otherwise it can make yr entry massive in the library and it'll look like you're trying to show off. 


