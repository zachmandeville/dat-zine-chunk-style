const html = require('nanohtml')

module.exports = (state, emit) => {
    if (state.info) {
       return html`
       <section id='back-cover'>
        <h1>Good Information</h1>
        <div id='info'>
          ${renderInfo(state.info)}
        </div>
        <section id='invite-to-copy'>
        <p>Wanna Make your own zine?  You can copy this one to use its structure, then  adjust it to match yr heart!</p>
        <button onclick=${makeCopy}>Make yr Own Copy!</button>
        </section>
       </section>
      ` 
    }

    function renderInfo (info) {
        var entries = Object.entries(info)
        return entries.map( entry => {
            return html`
             <p><strong>${entry[0]}</strong> ${entry[1]}</p>
            `
        })
    }

    async function makeCopy () {
        var self = new DatArchive(window.location)
        var branch = await DatArchive.fork(self, {title: 'name of your zine', description: 'make sure to read the README.  It helps!'})
        window.open(branch.url)
    }
}
