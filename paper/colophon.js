const html = require('nanohtml')
const _ = require('lodash')

module.exports = view

function view (state, emit) {
  if (state.info) {
    var info = state.info
    return html`
    <div id='colophon'>
    <div id='thank-you'>
    <p>Thank you for reading this zine. If you want to see information about a topic, let me know and I will try to know some information about it.</p>
    <p> Love, Ryland Duncan</p>
</div>
    </div>
      `
  } else {
    return html`<body><p>loading</p></body>`
  }
}
