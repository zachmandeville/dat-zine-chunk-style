const html = require('nanohtml')

module.exports = (state, emit) => {
  if (state.text.title) {
    state.index = 0 //If you get back to the cover, make sure the story starts over.
    var coverImage = `../aesthetic/cover-image/${state.coverImage}`
    var title = state.text.title.toUpperCase()

    return html`
    <div id='cover'>
        <h1>GOOD INFORMATION ABOUT ${title}</h1>
        <h2>By Ryland Duncan</h2>
    </div>
    `
  }
}
